const paymentCtrl = require('./app/controllers/v1/opay')
const grpc = require('grpc');


const PROTO_PATH = __dirname + '/proto/opay.proto';
const opayProto = grpc.load(PROTO_PATH).payment;


const server = new grpc.Server();


// rabbit.consume(uri,'transaction-add',paymentCtrl.AddTransaction)


server.addService(opayProto.Opay.service, paymentCtrl);
server.bind('0.0.0.0:50053', grpc.ServerCredentials.createInsecure());
server.start();
console.log('Server Start');




// const client = new opayProto.Opay('127.0.0.1:50053', grpc.credentials.createInsecure());






// client.ChargeId({id:'5b2cbb0c8d1dc30001241046'}, (err,resp)=>{
//     console.log(resp)
// })




// client.InitPayment({
//     cardNumber: '5399232154509969',
//     cardDateMonth: '07',
//     cardDateYear: '19',
//     cardCVC: '158',
//     amount: 100,
//     mobile: '2348141804018'
// }, (err, res) => {
//     if (err) {
//         console.log(err);
//     } else {
//         console.log(res);
//     }
// })


// client.Input_pin({token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtb2JpbGUiOiIyMzQ4MTQxODA0MDE4IiwiYW1vdW50IjoxMDAsInRva2VuIjoiNWIyY2JiMGM4ZDFkYzMwMDAxMjQxMDQ1IiwiaWF0IjoxNTI5NjU4MTI1fQ.wBY0qOac93Hv1sapDmOAF_zQmXLMjTFEzI89FG6m9tY",pin:'5555'},(err, resp)=>{
//     console.log(resp)
// })



// client.Input_otp({token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJtb2JpbGUiOiIyMzQ4MTQxODA0MDE4IiwiYW1vdW50IjoxMDAsInRva2VuIjoiNWIyY2JiMGM4ZDFkYzMwMDAxMjQxMDQ1IiwiaWF0IjoxNTI5NjU4MTY1fQ.COPrc7U-wFU_eoeJ9GURN6SEGGWO2QqxO9_IKw5X9do",otp:'361708'},(err, resp)=>{
//     console.log(resp)
// })



// client.TakeOut({mobile:'2348141804018', bank_code: '011', amount:200,  account_number:'3061930950'}, (err, resp)=>{
//     console.log(resp)
// } )

