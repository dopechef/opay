const rmdstr = require('randomstring');

exports.appendCountryCode = function countryCode(mobile) {
  return mobile.replace('0', '234');
};


exports.generateRandomNumberLen = function generateRandomNumberLen(length) {
  const token = rmdstr.generate({
    length,
    charset: '123456789',
  });
  return token;
};
