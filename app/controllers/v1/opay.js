const axios = require('axios');
const mongoose = require('mongoose');
const request  = require('request');
const jwt  = require('jsonwebtoken');

require('dotenv').config()

const grpc = require('grpc');

const PRORTO_PATH = __dirname + '/proto/wallet.proto';

const eyowoMain = grpc.load(PRORTO_PATH).eyowo_main;


const client = new eyowoMain.Main('0.0.0.0:50052', grpc.credentials.createInsecure());

const InitPayment = async (call, callback) => {
    try{
    req = call.request;
    
    const {amount, mobile} = req
    
    const response = await axios.post('https://operapay.com/api/gateway/create', {
        "input": {
            currency: "NGN",
            publicKey: process.env.opay_public,
            amount: req.amount,
            reference: mongoose.Types.ObjectId().toString(),
            countryCode: "NG",
            tokenize: true,
            instrumentType: "card",
            cardNumber: req.cardNumber,
            cardDateMonth: req.cardDateMonth,
            cardDateYear: req.cardDateYear,
            cardCVC: req.cardCVC,
        }
    })

    const data = response.data;
    const r = data.data.gatewayCreate

    if(!r){
        const  error = data.errors[0].message
      
         return callback(null,({success:false, data:JSON.stringify(error)}))
     }

     console.log(r.token)
     const jtoken = jwt.sign({mobile,amount,token:r.token},'cardsecret')

    if (r.status === 'successful') {

        client.fundWallet({amount, instrumentId: 'undefined',mobile},(err, resp)=>{
            if (err) throw err 
           
            return callback(null, { success: true, data: 'Transaction Successful' });
        })

        
    } else if (r.status === 'failed'){
        
        return callback(null, { success: false, data: JSON.stringify(r) });
    }else{
      
        r.token = jtoken
        return callback(null, { success: true, data: JSON.stringify(r) });
    }
    }catch(err) {
        console.log(err)
        return callback(null, { success: false, data: 'error   Initializing Data' });
    
    }

};

const Input_pin = async (call, callback) => {

    try{
        const req  = call.request
     
        const token = jwt.verify(String(req.token), 'cardsecret');
       
   const response = await  axios.post('https://operapay.com/api/gateway/input-pin', {
               "token": token.token,
               "pin": req.pin
           })

           const data = response.data;
           const r = data.data.gatewayInputPIN;
       
           if(!r){
              const  error = data.errors[0].message
            
               return callback(null,({success:false, data:JSON.stringify(error)}))
           }

           const jtoken = jwt.sign({mobile:token.mobile,amount:token.amount,token:token.token},'cardsecret')


           if (r.status === 'successful') {
               //call jerry
               client.fundWallet({amount:token.amount, instrumentId:r.instrumentId, mobile:token.mobile},(err, resp)=>{
                if (err) throw err 
                console.log(resp)
                return callback(null, { success: true, data: 'Transaction Successful' });
            })
            cons
           } else  if (r.status === 'failed') {
               //call jerry
               return callback(null, { success: false, data: 'Transaction failed' });
           }else{
               r.token = jtoken
               return callback(null, { success: true, data: JSON.stringify(r) });
           }
           }catch(err) {
               return callback(null, { success: false, data: 'error  Initializing Data' });
            console.log(err)
           }
       }

const Input_otp = async (call, callback) => {

        try{
            
        req = call.request;
        var token = jwt.verify(String(req.token), 'cardsecret');
      console.log(token)
            const response = await axios.post('https://operapay.com/api/gateway/input-otp',

                { "token": token.token, 
                  "otp": req.otp }

            )
                const data = response.data;

           
                const r = data.data.gatewayInputOTP;
                if(!r){
                    const  error = data.errors[0].message
                  
                     return callback(null,({success:false, data:JSON.stringify(error)}))
                 }
            
                if (r.status === 'successful') {
                    client.fundWallet({amount:token.amount, instrumentId:r.instrumentId, mobile:token.mobile},(err, resp)=>{
                        if (err) throw err 
                        console.log(resp)
                        return callback(null, { success: true, data: 'Transaction Successful' });
                    })
                    console.log(r)
                } else if (r.status === 'failed'){
        
                    return callback(null, { success: false, data: JSON.stringify(r) });
                }
                
                else {
                    return callback(null, { success: true, data: JSON.stringify(r) });
                }
                }catch(err) {
                    return callback(null, { success: false, data: 'error  Initializing Data' });
                }
}




const getAccessCode = async () => new Promise((resolve, reject) => {
    request.post('https://operapay.com/api/access/code', {
      json: true,
      body: {
        publicKey: process.env.opay_public,
      },
    }, (err, response, body) => {
      resolve(body.data.accessCode);
    });
  });
  
  
  const getToken = async () => new Promise(async (resolve, reject) => {
    request.post('https://operapay.com/api/access/token', {
      json: true,
      body: {
        accessCode: await getAccessCode(),
        privateKey: process.env.opay_private,
      },
    }, (err, response, body) => resolve(body.data.accessToken.value));
  });
  

  const ChargeId = async (call, callback) => {
      const req  = call.request
    request.post({
        url: "https://operapay.com/api/payment/order",
        json: true,
        headers: { "Authorization": await getToken() },
        body: {
            orderConfig: {
                serviceType: "gateway",
                paymentAmount: "100",
                currencyISO: "NGN",
                countryCode: "NG",
                instruments: [{type: "token", id: req.id}]
            }
        }
    }, (err, response, body) => {
        const r = body.data.orderForMerchant;
        console.log(body)
        if(!r){
            const  error = body.errors[0].message
          
             return callback(null,({success:false, data:JSON.stringify(error)}))
         }
    
         return callback(null,({success:true, data:JSON.stringify(r)}))
        
    });
  }
  

  const TakeOut = async (call, callback) => {
      try{

      const req = call.request
    request.post('https://operapay.com/api/payment/order', {
      json: true,
      headers: {
        Authorization: `token ${await getToken()}`
      },
      body: {
        orderConfig: {
          serviceType: 'bank',
          recipientAccount: req.account_number,
          recipientBankCode: req.bank_code,
          paymentAmount: req.amount,
          currencyISO: 'NGN',
          countryCode: 'NG',
          customerPhone: req.mobile,
          customerEmail: 'customer@email.com',
          instruments: [{
            type: 'coins'
          }],
        },
      },
    }, (err, response, body) => {
        const r = body.data.orderForMerchant;
        if(!r){
            const  error = body.errors[0].message
          
             return callback(null,({success:false, data:JSON.stringify(error)}))
         }
         return callback(null,({success:true, data:JSON.stringify(r)}))
    });
}catch(err){
    return callback(null, { success: false, data: 'error  Initializing Data' });
}
  };



module.exports = {InitPayment, Input_otp, Input_pin, ChargeId, TakeOut}