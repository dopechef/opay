const jwt = require('jsonwebtoken');
const config = require('../config/config.js');
const conn = require('../config/db.js');

const User = conn.models.user;
const Business = conn.models.business;
const Store = conn.models.store;
const Till = conn.models.till;
const Authorization = conn.models.authorization;

module.exports.validateSession = function validateSession(req, res, next) {
  // check to see that the auth token has been extracted
  // and necessary fields applied to the req object
  if (req.eyowo_user === undefined || req.eyowo_user._id === undefined || req.eyowo_user._id === null) {
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized',
    });
  }

  if (req.params.user_id !== req.eyowo_user._id.toString()) {
    console.log('NOT EQUAL');
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized; false header',
    });
  }
  console.log('here');
  // console.log(req.eyowo_user);
  // everything good, move on
  next();
};

// check to ensure that the authenticated user owns the business being operated on
module.exports.validateUserOwnsBusiness = async function validateUserOwnsBusiness(req, res, next) {
  // check to see that the auth token has been extracted
  // and necessary fields applied to the req object
  if (req.eyowo_user === undefined || req.eyowo_user._id === undefined || req.eyowo_user._id === null) {
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized',
    });
  }

  const business = await Business.findOne({ _id: req.params.business_id });


  if (business.owner.toString() !== req.eyowo_user._id.toString()) {
    console.log('NOT EQUAL');
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized; false header',
    });
  }
  console.log('here');
  // console.log(req.eyowo_user);
  // everything good, move on
  next();
};

// check to ensure that the authenticated user owns the store being operated on
module.exports.validateUserOwnsStore = async function validateUserOwnsStore(req, res, next) {
  // check to see that the auth token has been extracted
  // and necessary fields applied to the req object
  if (req.eyowo_user === undefined || req.eyowo_user._id === undefined || req.eyowo_user._id === null) {
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized',
    });
  }

  const store = await Store.findOne({ _id: req.params.store_id });


  if (store.owner.toString() !== req.eyowo_user._id.toString()) {
    console.log('NOT EQUAL');
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized; false header',
    });
  }
  console.log('here');
  // console.log(req.eyowo_user);
  // everything good, move on
  next();
};

// check to ensure that the authenticated user owns the store being operated on
module.exports.validateUserOwnsAuthorizationRequest = async function validateUserOwnsAuthorizationRequest(req, res, next) {
  // check to see that the auth token has been extracted
  // and necessary fields applied to the req object
  if (req.eyowo_user === undefined || req.eyowo_user._id === undefined || req.eyowo_user._id === null) {
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized',
    });
  }

  const authorization = await Authorization.findOne({ _id: req.params.authorization_id });

  if (!authorization) {
    console.log('DOES NOT EXIST');
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized; not found',
    });
  }

  if (authorization.user.toString() !== req.eyowo_user._id.toString()) {
    console.log('NOT EQUAL');
    return res.status(403).send({
      success: false,
      message: 'Request unauthorized; false header',
    });
  }
  console.log('here');
  // console.log(req.eyowo_user);
  // everything good, move on
  next();
};


module.exports.validateToken = function validateToken(req, res, next) {
  if (req.headers.authorization === undefined) {
    return res.status(403).send({
      success: false,
      message: 'No token provided.',
    });
  }
  let token;
  try {
    token = req.headers.authorization;
    // console.log(req.headers['authorization']);
    // console.log(req.headers);
    // console.log(token);
    token = token.substr(7);
  } catch (err) {
    return res.status(403).send({
      success: false,
      message: 'Invalid or no authorization token provided.',
    });
  }

  // decode token
  // verifies secret and checks exp
  jwt.verify(token, config.tokenSecret, (err, decoded) => {
    if (err) {
      return res.status(403).send({
        success: false,
        message: 'Failed to authenticate token.',
        err,
      });
    }
    User.findById(decoded._id, (errr, user) => {
      if (errr) {
        console.log('nothing found');
        return res.status(403).send({
          success: false,
          message: 'Authentication failed. Eyowo user not found.',
        });
      }
      // if everything is good, save to request for use in other routes
      req.token = decoded;
      req.eyowo_user = user;

      // console.log(req.eyowo_user);

      next();
    });
  });
  return null;
};

module.exports.validateTokenTill = function validateToken(req, res, next) {
  if (req.headers.authorization === undefined) {
    return res.status(403).send({
      success: false,
      message: 'No token provided.',
    });
  }
  let token;
  try {
    token = req.headers.authorization;
    // console.log(req.headers['authorization']);
    // console.log(req.headers);
    // console.log(token);
    token = token.substr(7);
  } catch (err) {
    return res.status(403).send({
      success: false,
      message: 'Invalid or no authorization token provided.',
    });
  }

  // decode token
  // verifies secret and checks exp
  jwt.verify(token, config.tokenSecret, (err, decoded) => {
    if (err) {
      return res.status(403).send({
        success: false,
        message: 'Failed to authenticate token.',
        err,
      });
    }
    Till.findById(decoded._id, (errr, till) => {
      if (errr) {
        console.log('nothing found');
        return res.status(403).send({
          success: false,
          message: 'Authentication failed. Eyowo user not found.',
        });
      }
      // if everything is good, save to request for use in other routes
      req.token = decoded;
      req.till = till;

      // console.log(req.eyowo_user);

      next();
    });
  });
  return null;
};

module.exports.validateSetSecurePinToken = function validateSetSecurePinToken(req, res, next) {
  if (req.headers.authorization === undefined) {
    return res.status(403).send({
      success: false,
      message: 'No token provided.',
    });
  }
  let token;
  try {
    token = req.headers.authorization;
    // console.log(req.headers['authorization']);
    // console.log(req.headers);
    // console.log(token);
    token = token.substr(7);
  } catch (err) {
    return res.status(403).send({
      success: false,
      message: 'Invalid or no authorization token provided.',
    });
  }

  // decode token
  // verifies secret and checks exp
  jwt.verify(token, config.tokenSecret, (err, decoded) => {
    if (err) {
      return res.status(403).send({
        success: false,
        message: 'Failed to authenticate token.',
        err,
      });
    }
    if (decoded.usage === undefined) {
      return res.status(403).send({
        success: false,
        message: 'Invalid token rights.',
      });
    }
    if (decoded.usage !== 'setpin') {
      return res.status(403).send({
        success: false,
        message: 'Token cannot perform this action.',
      });
    }
    /*
    User.findById(decoded._id, (errr, user) => {
      if (errr) {
        console.log('nothing found');
        return res.status(403).send({
          success: false,
          message: 'Authentication failed. Eyowo user not found.',
        });
      }
      // if everything is good, save to request for use in other routes
      req.token = decoded;
      req.eyowo_user = user;

      // console.log(req.eyowo_user);

      next();
    }); */
    req.set_securepin_token = decoded;

    // console.log(req.eyowo_user);

    next();
  });
  return null;
};
