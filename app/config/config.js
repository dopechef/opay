module.exports = {
  port: process.env.PORT || 8080,
  secret: process.env.SECRET,
  tokenSecret: process.env.TOKENSECRET,
  cipherSecret: process.env.CIPHERSECRET,
  smshttpoptions: {
    host: 'api.ebulksms.com',
    path: '/sendsms.json',
    port: '8080',
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
  },
};
